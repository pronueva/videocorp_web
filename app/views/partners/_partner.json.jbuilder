json.extract! partner, :id, :name, :url, :phone, :logo, :order, :created_at, :updated_at
json.url partner_url(partner, format: :json)