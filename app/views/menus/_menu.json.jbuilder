json.extract! menu, :id, :name, :url, :menu_id, :status, :created_at, :updated_at
json.url menu_url(menu, format: :json)