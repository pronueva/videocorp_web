json.extract! project, :id, :name, :description, :project_type, :images, :principal_img, :created_at, :updated_at,
json.url project_url(project, format: :json)