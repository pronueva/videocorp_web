json.extract! service, :id, :name, :service_id, :description, :image, :category_id_id, :created_at, :updated_at
json.url service_url(service, format: :json)