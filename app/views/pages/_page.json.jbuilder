json.extract! page, :id, :title, :slug, :content, :status, :created_at, :updated_at
json.url page_url(page, format: :json)