json.extract! category, :id, :nname, :category_id, :desciption, :slug, :created_at, :updated_at
json.url category_url(category, format: :json)