class Category < ActiveRecord::Base
  self.primary_key = 'id'

  paginates_per 6

  has_attached_file :image , styles: {principal: "286x258", thumb: "100x100"}, default_url: "missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/


  validates :slug, presence: true

  def childrens(id)
    @childrens = Category.where(category_id:  id)
  end
end
