class Brand < ActiveRecord::Base
  self.primary_key = 'id'
  paginates_per 10
  has_many :products

end
