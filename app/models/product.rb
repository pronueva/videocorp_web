class Product < ActiveRecord::Base
  self.primary_key = 'id'

  paginates_per 9

  validates :price, numericality: {greather_than: 0}

  has_many :images, dependent: :destroy
  belongs_to :brand

  has_attached_file :avatar ,
                    styles: {medium: "300x200", thumb: "100x100", icon: "50x50"},
                    default_url: "missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  has_attached_file :pdf_file
  validates_attachment :pdf_file, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }
end
