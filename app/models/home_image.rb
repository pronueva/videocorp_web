class HomeImage < ActiveRecord::Base
  self.primary_key = 'id'
  has_attached_file :home_image ,
                    styles: {medium: "300x280", thumb: "150x140", icon: "75x70"},
                    default_url: "missing.png"
  validates_attachment_content_type :home_image, content_type: /\Aimage\/.*\Z/
end
