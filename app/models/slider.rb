class Slider < ActiveRecord::Base
  paginates_per 10
  self.primary_key = 'id'

  has_many :images, dependent: :destroy
  has_many :slider_images, dependent: :destroy
  has_many :slider_videos, dependent: :destroy

  has_attached_file :image ,
                    styles: {slider: "1920x800", medium: "1200x600", bottom: "1000x400",small: "600x315" ,thumb: "300x152"},
                    default_url: "missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

end
