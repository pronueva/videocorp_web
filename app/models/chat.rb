class Chat < ActiveRecord::Base
  self.primary_key = 'id'
  has_many :chat_messages
  belongs_to :customer
  belongs_to :user

end
