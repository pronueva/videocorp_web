class Page < ActiveRecord::Base
  paginates_per 10

  belongs_to :slider
  self.primary_key = 'id'
end
