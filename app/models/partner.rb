class Partner < ActiveRecord::Base
  self.primary_key = 'id'

  paginates_per 10

  has_attached_file :logo ,
                    styles: {medium: "300x200", thumb: "100x100", icon: "50x50"},
                    default_url: "missing.png"
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/
end
