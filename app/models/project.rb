class Project < ActiveRecord::Base
  self.primary_key = 'id'

  def previous_project
    Project.where(["id < ? AND project_category_id = 1" , id]).order("id desc").first
  end
  def next_project
    Project.where(["id > ? AND project_category_id = 1" , id]).order("id asc").first
  end
  def previous_exec_project
    Project.where(["id < ? AND project_category_id = 2" , id]).order("id desc").first
  end
  def next_exec_project
    Project.where(["id > ? AND project_category_id = 2" , id]).order("id asc").first
  end

  paginates_per 5

  belongs_to :project_category
  has_many :images, dependent: :destroy

  has_attached_file :avatar_projects ,
                    styles: {large: "800x600" ,medium: "400x300", thumb: "200x100", icon: "100x50"},
                    default_url: "missing.png"
  has_attached_file :customer_logo ,
                    styles: {large: "800x600" ,medium: "400x300", thumb: "200x100", icon: "100x50"},
                    default_url: "logo.png"

  validates_attachment_content_type :avatar_projects, content_type: /\Aimage\/.*\Z/
end
