class Applicant < ActiveRecord::Base
  self.primary_key = 'id'

  has_attached_file :pdf_file
  validates_attachment :pdf_file, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }

end
