class SliderImage < ActiveRecord::Base
  paginates_per 10
  self.primary_key = 'id'

  belongs_to :slider

  scope :order_asc, -> { order("slider_images.order ASC") }


  has_attached_file :image ,
                    styles: {home_slider: "1920x800", bottom_slider: "1000x200", thumb: "100x50"},
                    default_url: "missing.png"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  def youtube_embed_url
    return 'https://youtube.com/embed/'+ youtube_video_id
  end

  def youtube_video_id
    video_id = self.video_url.split('v=')[1]
    return video_id.to_s
  end
end
