class Menu < ActiveRecord::Base
  paginates_per 10
  self.primary_key = 'id'


  def child_menu(menu_id)
    Menu.where(["menu_id = ?" , menu_id]).order("id desc").all
  end

end
