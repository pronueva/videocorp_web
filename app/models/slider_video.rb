class SliderVideo < ApplicationRecord
  paginates_per 10
  self.primary_key = 'id'

  belongs_to :slider

  scope :order_asc, -> { order("slider_videos.order ASC") }

  has_attached_file :video, :styles => {
      :medium => { :geometry => "640x480", :format => 'flv' },
      :thumb => { :geometry => "100x100#", :format => 'jpg', :time => 10 }
  }, :processors => [:ffmpeg]

  validates_attachment_content_type :video, :content_type => /\Avideo\/.*\Z/

  def youtube_embed_url
    video_url = self.video_url.split('v=')[1]
    return 'https://youtube.com/embed/'+video_url.to_s
  end

  def youtube_video_id
    video_id = self.video_url.split('v=')[1]
    return video_id.to_s
  end
end
