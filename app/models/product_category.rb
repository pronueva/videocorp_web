class ProductCategory < ActiveRecord::Base
  belongs_to :category
  belongs_to :product
  self.primary_key = 'id'
  paginates_per 10
end
