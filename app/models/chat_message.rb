class ChatMessage < ActiveRecord::Base
  self.primary_key = 'id'
  belongs_to :chat
end
