class Image < ActiveRecord::Base
    self.primary_key = 'id'

    belongs_to :product
    belongs_to :project

    has_attached_file :image,
                      styles: {medium: "311x249", baja: "65x50", icon: "50x50",large:"800x600"},
                      default_url: "missing.png"

    do_not_validate_attachment_file_type :image

end
