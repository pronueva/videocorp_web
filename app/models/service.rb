class Service < ActiveRecord::Base
  belongs_to :service_category
  self.primary_key = 'id'

  paginates_per 6

  has_attached_file :image , styles: {principal: "286x258", medium: "300x300", thumb: "100x100", icon: "50x50"}, default_url: "missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

end
