class ContactMailer < ApplicationMailer
  default from: "contactos@videocorp.com"

  def sample_email(params)
    @name = params[:name]
    @email = params[:email]
    @phone = params[:phone]
    @msg = params[:mensaje]
    @sucursal = params[:sucursal].to_s.capitalize
    if @sucursal === "CHILE"
      mail(to: "contacto@videocorp.com", subject: 'Videocorp - [Contacto - '+ @sucursal +']')
    end
    if @sucursal === "PERU"
      mail(to: "ventasperu@videocorp.com", subject: 'Videocorp - [Contacto - '+ @sucursal +']')
    end
    if @sucursal === "COLOMBIA"
      mail(to: "vmatamala@videocorp.com", subject: 'Videocorp - [Contacto - '+ @sucursal +']')
    end
  end
end
