class AdminController < ApplicationController
  layout "admin_layout"
  before_action :set_path
  before_action :set_categories
  before_action :set_user
  require 'timeout'

  def set_user
      @pending_chats = Chat.where(status: "2")
  end

  def set_categories
    @categories = Category.where(category_id: "0")
  end

  def set_path
    @path = "admin"
    @section = "Dashboard"
  end
  def home
    @products = Product.all
    @categories = Category.all
    @pages = Page.where("status = 1").order("principal_page DESC")
    @p_numbers = PageNumber.all
    @home_page = Page.find(1)
    @home_images = HomeImage.all
    @projects_e = Project.where("project_category_id = 1")
    @projects = Project.where("project_category_id = 2")
  end
  def home_page
    @path = "home"
    @p_numbers = PageNumber.all
    @home_page = Page.find(1)
    @home_images = HomeImage.all
    @home_texts = HomeText.find(1)
  end
  def numbers_update
    @p_number = PageNumber.find(params[:id])
    @p_number.name = params[:name]
    @p_number.number = params[:number]
    @p_number.save!
    redirect_to controller: "admin", action: "home_page"
  end
  def home_images_update
    @home_images = HomeImage.find(params[:id])
    params[:home_image] ? @home_images.home_image = params[:home_image] : ''
    @home_images.title = params[:title]
    @home_images.url = params[:url]
    @home_images.save!
    redirect_to controller: "admin", action: "home_page"
  end
  def home_texts_update
    @home_texts = HomeText.find(1)
    @params = params[:home_texts]
    @home_texts.title = @params["title"]
    description = @params["description"].gsub("\n","<br/>")
    @home_texts.description = description
    @home_texts.save!
    redirect_to controller: "admin", action: "home_page"
  end
  def home_slider_update
    @home_page = Page.find(1)
    @home_page.slider_id = params[:slider_id]
    @home_page.save!

    redirect_to controller: "admin", action: "home_page"
  end
  def home_promo_slider_update
    @home_page = Page.find(1)
    @home_page.promo_slider_id = params[:slider_id]
    @home_page.save!

    redirect_to controller: "admin", action: "home_page"
  end

  def auth
    redirect_to new_user_session_path
  end

  def chat
    @path = "chat"
    @us = current_user
    render "admin/chat"
  end

  def change_status
    @status = params[:val]
    @user = current_user
    @user.chat_user_status = @status
    @user.save!
    @path = "chat"
    render "admin/chat"
  end

  def refresh_table
    @path = "chat"

    render "admin/_chatTable", :layout => false
  end

end
