class UsersController <   Devise.parent_controller.constantize
  layout "admin_layout"
  before_action :set_path
  before_action :set_categories

  def set_categories
    @categories = Category.where(category_id: 0)
  end
  def set_path
    @path = "users"
    @section = "Usuarios"
  end

  def index
    if params[:search]
      s = params[:search]
      @users = User.where("user_type_id < ? AND name LIKE ?", 5,"%#{s}%")
    else
      @users = User.where(["user_type_id < :user_type_id",{user_type_id: 5}])
    end
  end
  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    @user_types = UserType.all
  end
  # GET /users/new
  def new
    @title = 'Crear Nuevo Usuario'
    @action = 'new'
    @user_types = UserType.all
    @user = User.new
  end
  # GET /users/1/edit
  def edit
    @user_types = UserType.all
    @action = 'edit'
    @title = 'Editar Usuario'
    @user = User.find(params[:id])
  end
  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'El usuario ha sido creado correctamente.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user_types = UserType.all
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'El usuario ha sido editado correctamente.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'El usuario ha sido eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :last_name, :email, :password, :email, :user_type_id)
    end
end
