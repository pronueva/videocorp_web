class WelcomeController < ApplicationController
  before_action :set_categories
  before_action :set_project_categories
  before_action :set_videocorp_partners
  before_action :set_brands
  before_action :set_interior_pages
  add_breadcrumb "Home", :root_path, :except => [:index]

  def set_interior_pages
    @interior_pages = Page.where("principal_page = 0 AND status != 3")
    @footer_pages = Page.where("in_footer = 1 AND status != 3")
    @principal_page = Page.where("principal_page = 1").first

    @menu = Menu.where("status = 1").order("id ASC").all

    @pejecutados = 0
    @pejecucion = 0
    @product_class = 0
  end
  def set_brands
    @brands = Brand.all
  end
  def set_categories
    @categories = Category.where(category_id: 0)
    @product_categories = Category.where.not(category_id: 0)
  end
  def set_project_categories
    @categories = Category.where(category_id: 0)
    @product_categories = Category.where.not(category_id: 0)
  end

  def set_videocorp_partners
    @videocorp_partners = Partner.where("is_partner = 1")
  end
  def set_home_slider(slider_id)
    @home_slider = Slider.find(slider_id)
  end
  def set_promo_slider(slider_id)
    @promo_slider = Slider.find(slider_id)
  end
  def set_numbers
    @numbers = PageNumber.all
  end

  def index
    @no_breadcrumb = true
    @path = "home"
    @home_page = Page.find(1)
    @home_images = HomeImage.all()
    set_home_slider(@home_page.slider_id)
    set_promo_slider(@home_page.promo_slider_id)
    set_numbers
  end

  # GET /products
  def search_products
    @page = Page.new
    @page.title = "Productos"

    @product_class = 1
    add_breadcrumb "Productos" , 'productos'
    @category = 0

    @brands = Brand.all
    if params[:brand] != "0"
      @selected_brand = Brand.find(params[:brand])
      @products = Product.where(brand_id: @selected_brand.id).page params[:page]
    else
      @selected_brand = Brand.new
      @selected_brand.id = 0

      @products = Product.all.page params[:page]
    end
    @content = ''
    render "search_product"
  end

  def productos
    @page = Page.new
    @page.title = "Productos"

    @product_class = 1
    add_breadcrumb "Productos" , 'productos'

    @selected_brand = Brand.new
    @selected_brand.id = 0

    if params[:brand]
      @brand = Brand.find_by_name(params[:brand])
      @products = Product.where("brand_id = ?",@brand.id).page params[:page]
    else
      @products = Product.all.page params[:page]
    end

    render "search_product"
  end

  def contacto
    @page = Page.new
    @page.title = "Contacto"

    @path = 'contacto'
    add_breadcrumb "Contacto" , '../contacto/'+params[:sucursal]

    @sucursal = params[:sucursal]
    @direccion = @sucursal == "chile" ?  'Santiago: Av Boulevard Aeropuerto 9646, Pudahuel, ENEA <br> Antofagasta: Los Manzanos N° 8805, Población las Rocas, Antofagasta <br> Concepción: Lincoyán 41-A'.html_safe : @sucursal == "peru" ?   'Calle 2 de Mayo 1362, Miraflores 15074, Perú' : @sucursal == "colombia" ?  'Calle 85A # 28b-21 , Barrio el Polo, Bogotá' : @sucursal == "brasil" ?  'Conselheiro Rodrigues Alves 180, Sao Paulo' : @sucursal == "usa" ? '9990 north west NW 14 st, Miami , Florida' :  'Londres 75, Juarez Ciudad de Mexico . zip: 06600'
    @telefono = @sucursal == "chile" ? 'Santiago: (56-2) 2431 61 00 <br> Antofagasta: (56 - 55) 247 9549 / 224 4332 <br>Concepción: (41) 2887230 '.html_safe : @sucursal == "peru" ? '(511) 447 6525' : @sucursal == "colombia" ? '' : @sucursal == "brasil" ? '' : @sucursal == "usa" ? '' : ''

    render "welcome/contacto"
  end
  def send_contact
    ContactMailer.sample_email(params).deliver
    @page = Page.new
    @page.title = "Contacto"
    @sucursal = params[:sucursal]
    if @sucursal == "santiago" || @sucursal.to_s == "antofagasta" || @sucursal.to_s == "concepcion"
        @sucursal = "chile"
    end
    redirect_to "/contacto/" + @sucursal.to_s , notice: "Se ha enviado su mensaje, Gracias."
  end

  def work_with_us
    @page = Page.new
    @page.title = "Trabaja con nosotros"

    @path = 'trabaja-con-nosotros'
    add_breadcrumb "Trabaja con nosotros" , '../trabaja-con-nosotros'

    render "welcome/work_with_us"
  end
  def send_work_with_us
    @path = 'trabaja-con-nosotros'
    add_breadcrumb "Trabaja con nosotros" , '../trabaja-con-nosotros'

    @applicant = Applicant.new(applicant_params)

      if @applicant.save
        ContactMailer.applicants_email(applicant_params).deliver
        @err = 0
        @notice = "Se ha enviado su solicitud, Gracias."

      else
        @err = 1
        @notice = "Ha ocurrido un error, intentelo más tarde, Gracias."

      end

    redirect_to "/trabaja-con-nosotros" , notice: "Se ha enviado su solicitud, Gracias."
  end


  def product
    @page = Page.new
    @page.title = @product.name

    @product_class = 1
    url = params[:brand]+'/'+params[:product_url]
    @product = Product.find_by_url(url)
    add_breadcrumb "Productos" , '../../productos'
    add_breadcrumb  @product.name
    @product_images = Image.where(product_id: @product.id)
    @comments = Comment.where(product_id: @product.id).order(created_at: :desc)
  end

  def proyectos_ejecucion
    @page = Page.new
    @page.title = "Proyectos en ejecución"

    @pejecucion = 1
    add_breadcrumb "Proyectos en ejecución" , 'proyectos-ejecucion'
    if params[:category]
      @category = params[:category]
      @category = Category.find(params[:category])
    else
      @category = Category.new
      @category.id = 0
    end

    @search_id = 1
    @title = 'Proyectos'
    @projects = Project.where(project_category_id: '1').page params[:page]
    @featured_projects = Project.where(["is_featured =  1 AND project_category_id = 1"])
    @project_categories = Category.all

    render "welcome/proyectos"
  end
  def proyectos_ejecutados
    @page = Page.new
    @page.title = "Proyectos ejecutados"

    @pejecutados = 1
    add_breadcrumb "Proyectos ejecutados" , 'proyectos-ejecutados'
    if params[:category]
      @category = params[:category]
      @category = Category.find(params[:category])
    else
      @category = Category.new
      @category.id = 0
    end

    @search_id = 2
    @title = 'Proyectos'
    @projects = Project.where(project_category_id: '2').order(created_at: :desc).page params[:page]
    @featured_projects = Project.where(["is_featured =  1 AND project_category_id = 2"])
    @project_categories = Category.all

    render "welcome/proyectos"
  end
  def show_proyecto
    @project = Project.where(url: params[:project_url]).first

    @page = Page.new
    @page.title = @project.name

    @project_images = Image.where(project_id: @project.id)
    if @project.project_category_id == 1
      @pejecucion = 1
      add_breadcrumb "Proyectos en ejecución" , ' ../proyectos-ejecucion'
    else
      @pejecutados = 1
      add_breadcrumb "Proyectos ejecutados" , '../proyectos-ejecutados'
    end
    add_breadcrumb  @project.name

    render "welcome/proyecto"
  end

  # GET /projects
  def search_projects
    @page = Page.new
    @page.title = "Proyectos"

    @project_ids = Array.new()
    @project_categories = Category.all
    @search_id = params[:sid]

    if !params[:keywords].nil?
      @keywords = params[:keywords]
      @projects = Project.where("name LIKE ? OR description LIKE ?","%#{@keywords}%", "%#{@keywords}%").page params[:page]
      add_breadcrumb "Busqueda: "+@keywords
    else
      @category = Category.find(params[:category])
      @title = @category.name
      @cat_projects = Project.where("involved_areas = :category_id AND project_category_id = :project_category_id",{category_id:params[:category],project_category_id: @search_id})
      if @cat_projects != nil
        @cat_projects.each do |cp|
           @project_ids <<  cp.id
        end
      else
        @project_ids = @category.id
      end

      if @search_id == "1"
        @pejecucion = 1
        add_breadcrumb "Proyectos en ejecución" , '../proyectos-ejecucion'
      else
        @pejecutados = 1
        add_breadcrumb "Proyectos ejecutados" , '../proyectos-ejectuados'
      end
        @projects = Project.where(id: @project_ids).page params[:page]
    end
    @search_id = 2
    @content = ''

    render "welcome/proyectos"
  end


  def partners
    @page = Page.new
    @page.title = 'Holding'

    add_breadcrumb "Holding" , '../holding'

    @partners = @videocorp_partners
    @ma = Partner.where("is_partner = 0")
    @marcas_asociadas = ''
    @vp = ''

    @ma.each do |ma|
      @marcas_asociadas << '<div class="col-md-3 partner-logo">
                            <a href="#"><img src="'+ma.logo.url(:medium)+'")" alt="'+ma.name+'"></a>
                           </div>'
    end
    @partners.each do |p|
      @vp <<  '<div class="small-padding" style="padding:5px 2em;width:20%;float:left">
                  <a href="'+p.url+'"><img style="width:100%" src="'+p.logo.url(:medium)+'" alt="'+p.name+'"></a>
                 </div>'
    end
    @page.content = '
    <div class="col-md-12 tab-partners">
    '+@vp+'    <br><br> <br><br> <br><br>
     <h2 class="text-center"> Marcas Asociadas </h2><br><br>
     '+@marcas_asociadas+'</div>'

    @no_slider = 1


    render "welcome/interior_page"
  end

  def interior_pages
    @page = Page.find_by_slug(params[:page_name])
    if params[:sub_page]
      @page = Page.find_by_slug(params[:page_name]<<'/'<<params[:sub_page])
    end

    @action = 'interior_pages'
    add_breadcrumb @page.title , '../'+@page.slug
    @slider = Slider.find(@page.slider_id)
    if @page.slug == 'eventos'
      @no_slider = 1
    end

    render "welcome/interior_page"
  end

  def update_project_urls
    @projects = Project.all
    @projects.each do |p|
      @project = p
      name = @project.name.gsub(' ','-').gsub('.','')
      url = name.to_s + '-' + @project.id.to_s
      @project.url = url
      @project.save!
    end

    @products = Product.all
    @products.each do |p|
      @product = p
      name = @product.name.gsub(' ','-').gsub('.','')
      brand = Brand.find(@product.brand_id)
      url = brand.name + '/' + name.to_s + '-' + @product.id.to_s
      @product.url = url
      @product.save!
    end

    self.index
  end

  private
  def applicant_params
    params.require(:applicant).permit(:name, :phone, :email, :rut, :work_area,:pdf_file)
  end

end
