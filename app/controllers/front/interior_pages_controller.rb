class Front::InteriorPagesController < HomeController

  def interior_pages
    @page = Page.find_by_slug(params[:page_name])
    if params[:sub_page]
      @page = Page.find_by_slug(params[:page_name]<<'/'<<params[:sub_page])
    end

    @action = 'interior_pages'
    add_breadcrumb @page.title , '../'+@page.slug
    @slider = Slider.find(@page.slider_id)
    if @page.slug == 'eventos'
      @no_slider = 1
    end

    render "welcome/interior_page"
  end
end
