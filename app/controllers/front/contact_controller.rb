class Front::ContactController < HomeController
  def contacto
    @page = Page.new
    @page.title = "Contacto"

    @path = 'contacto'
    add_breadcrumb "Contacto" , '../contacto'

    render "welcome/contacto"
  end
  def send_contact
    ContactMailer.sample_email(params).deliver
    @page = Page.new
    @page.title = "Contacto"
    @sucursal = params[:sucursal]
    redirect_to "/contacto/" , notice: "Se ha enviado su mensaje, Gracias."
  end

  def work_with_us
    @page = Page.new
    @page.title = "Trabaja con nosotros"

    @path = 'trabaja-con-nosotros'
    add_breadcrumb "Trabaja con nosotros" , '../trabaja-con-nosotros'

    render "welcome/work_with_us"
  end
  def send_work_with_us
    @path = 'trabaja-con-nosotros'
    add_breadcrumb "Trabaja con nosotros" , '../trabaja-con-nosotros'

    @applicant = Applicant.new(applicant_params)

    if @applicant.save
      ContactMailer.applicants_email(applicant_params).deliver
      @err = 0
      @notice = "Se ha enviado su solicitud, Gracias."

    else
      @err = 1
      @notice = "Ha ocurrido un error, intentelo más tarde, Gracias."

    end

    redirect_to "/trabaja-con-nosotros" , notice: "Se ha enviado su solicitud, Gracias."
  end

end
