class Front::ProjectsController < HomeController
  def proyectos_ejecucion
    @page = Page.new
    @page.title = "Proyectos en ejecución"

    @pejecucion = 1
    add_breadcrumb "Proyectos en ejecución" , 'proyectos-ejecucion'
    if params[:category]
      @category = params[:category]
      @category = Category.find(params[:category])
    else
      @category = Category.new
      @category.id = 0
    end

    @search_id = 1
    @title = 'Proyectos'
    @projects = Project.where(project_category_id: '1').page params[:page]
    @featured_projects = Project.where(["is_featured =  1 AND project_category_id = 1"])
    @project_categories = Category.all

    render "welcome/proyectos"
  end
  def proyectos_ejecutados
    @page = Page.new
    @page.title = "Proyectos ejecutados"

    @pejecutados = 1
    add_breadcrumb "Proyectos ejecutados" , 'proyectos-ejecutados'
    if params[:category]
      @category = params[:category]
      @category = Category.find(params[:category])
    else
      @category = Category.new
      @category.id = 0
    end

    @search_id = 2
    @title = 'Proyectos'
    @projects = Project.where(project_category_id: '2').order(created_at: :desc).page params[:page]
    @featured_projects = Project.where(["is_featured =  1 AND project_category_id = 2"])
    @project_categories = Category.all

    render "welcome/proyectos"
  end
  def show_proyecto
    @project = Project.where(url: params[:project_url]).first

    @page = Page.new
    @page.title = @project.name

    @project_images = Image.where(project_id: @project.id)
    if @project.project_category_id == 1
      @pejecucion = 1
      add_breadcrumb "Proyectos en ejecución" , ' ../proyectos-ejecucion'
    else
      @pejecutados = 1
      add_breadcrumb "Proyectos ejecutados" , '../proyectos-ejecutados'
    end
    add_breadcrumb  @project.name

    render "welcome/proyecto"
  end

  # GET /projects
  def search_projects
    @page = Page.new
    @page.title = "Proyectos"

    @project_ids = Array.new()
    @project_categories = Category.all
    @search_id = params[:sid]

    if !params[:keywords].nil?
      @keywords = params[:keywords]
      @projects = Project.where("name LIKE ? OR description LIKE ?","%#{@keywords}%", "%#{@keywords}%").page params[:page]
      add_breadcrumb "Busqueda: "+@keywords
    else
      @category = Category.find(params[:category])
      @title = @category.name
      @cat_projects = Project.where("involved_areas = :category_id AND project_category_id = :project_category_id",{category_id:params[:category],project_category_id: @search_id})
      if @cat_projects != nil
        @cat_projects.each do |cp|
          @project_ids <<  cp.id
        end
      else
        @project_ids = @category.id
      end

      if @search_id == "1"
        @pejecucion = 1
        add_breadcrumb "Proyectos en ejecución" , '../proyectos-ejecucion'
      else
        @pejecutados = 1
        add_breadcrumb "Proyectos ejecutados" , '../proyectos-ejectuados'
      end
      @projects = Project.where(id: @project_ids).page params[:page]
    end
    @search_id = 2
    @content = ''

    render "welcome/proyectos"
  end
  def update_project_urls
    @projects = Project.all
    @projects.each do |p|
      @project = p
      name = @project.name.gsub(' ','-').gsub('.','')
      url = name.to_s + '-' + @project.id.to_s
      @project.url = url
      @project.save!
    end

    @products = Product.all
    @products.each do |p|
      @product = p
      name = @product.name.gsub(' ','-').gsub('.','')
      brand = Brand.find(@product.brand_id)
      url = brand.name + '/' + name.to_s + '-' + @product.id.to_s
      @product.url = url
      @product.save!
    end

    self.index
  end
end
