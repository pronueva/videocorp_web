class Front::PartnersController < HomeController
  def holding
    @page = Page.new
    @page.title = 'Holding'

    add_breadcrumb "Holding" , '../holding'

    @partners = @videocorp_partners
    @ma = Partner.where("is_partner = 0")
    @marcas_asociadas = ''
    @vp = ''

    @ma.each do |ma|
      @marcas_asociadas << '<div class="col-md-3 partner-logo">
                            <a href="#"><img src="'+ma.logo.url(:medium)+'")" alt="'+ma.name+'"></a>
                           </div>'
    end
    @partners.each do |p|
      @vp <<  '<div class="holding-brand">
                  <a href="'+p.url+'"><img style="width:100%" src="'+p.logo.url(:medium)+'" alt="'+p.name+'"></a>
                 </div>'
    end
    @page.content = '
    <div class="holding">'+@vp+'</div>
    <div class="row tab-partners">
      <h2 class="col-12 text-center associated-brands"> Marcas Asociadas </h2>
     '+@marcas_asociadas+'</div>'
    @no_slider = 1

    render "welcome/interior_page"
  end
end
