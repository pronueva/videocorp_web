class ImagesController < ApplicationController
  # GET /pictures
  # GET /pictures.json
  def index
    @product = Product.find(params[:product_id])

    @image = @product.images

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @image }
    end
  end

  # GET /pictures/1
  # GET /pictures/1.json
  def show
    @image = Image.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb.erb
      format.json { render json: @image }
    end
  end

  # GET /pictures/new
  # GET /pictures/new.json
  def new
    @product = Product.find(params[:product_id])
    @image = @product.images.build

    respond_to do |format|
      format.html # _form.html.erb
      format.json { render json: @image }
    end
  end

  # GET /pictures/1/edit
  def edit
    #@gallery = Gallery.find(params[:gallery_id])

    @image = Image.find(params[:id])
    # @picture = Picture.find(params[:id])
  end

  # POST /pictures
  # POST /pictures.json
  def create
    @image = Image.new(params[:image])

    if @image.save
      respond_to do |format|
        format.html {
          render :json => [@image.to_jq_upload].to_json,
                 :content_type => 'text/html',
                 :layout => false
        }
        format.json {
          render :json => [@image.to_jq_upload].to_json
        }
      end
    else
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  # PUT /pictures/1
  # PUT /pictures/1.json
  def update

    @product = Product.find(params[:product_id])

    @image = @product.images.find(params[:id])

    respond_to do |format|
      if @picture.update_attributes(image_params)
        format.html { redirect_to product_path(@product), notice: 'Picture was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pictures/1
  # DELETE /pictures/1.json
  def destroy
    #@gallery = Gallery.find(params[:gallery_id])
    #@picture = @gallery.pictures.find(params[:id])
    @image = Image.find(params[:id])
    @image.destroy

    respond_to do |format|
      format.html { redirect_to root_path }
      format.js
    end
  end

  def make_default
    @image = Image.find(params[:id])
    @product = Product.find(params[:product_id])

    @product.cover = @image.id
    @product.save

    respond_to do |format|
      format.js
    end
  end

  private

  def image_params
    params.require(:iamge).permit(:description, :product_id, :image)
  end


end