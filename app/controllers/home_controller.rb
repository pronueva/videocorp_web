class HomeController < ApplicationController
  before_action :set_categories
  before_action :set_project_categories
  before_action :set_videocorp_partners
  before_action :set_brands
  before_action :set_interior_pages
  add_breadcrumb "Home", :root_path, :except => [:index]
  layout "new_layout"

  def set_interior_pages
    @interior_pages = Page.where("principal_page = 0 AND status != 3")
    @footer_pages = Page.where("in_footer = 1 AND status != 3")
    @principal_page = Page.where("principal_page = 1").first
    @menu = Menu.where("status = 1").order("id ASC").all
    @pejecutados = 0
    @pejecucion = 0
    @product_class = 0
  end
  def set_brands
    @brands = Brand.all
  end
  def set_categories
    @categories = Category.where(category_id: 0)
    @product_categories = Category.where.not(category_id: 0)
  end
  def set_project_categories
    @categories = Category.where(category_id: 0)
    @product_categories = Category.where.not(category_id: 0)
  end

  def set_videocorp_partners
    @videocorp_partners = Partner.where("is_partner = 1")
  end
  def set_home_slider(slider_id)
    @home_slider = Slider.find(slider_id).slider_images.order_asc
  end
  def set_promo_slider(slider_id)
    @promo_slider = Slider.find(slider_id)
  end
  def set_numbers
    @numbers = PageNumber.all
  end
  def index
    @no_breadcrumb = true
    @path = "home"
    @home_page = Page.find(1)
    @home_images = HomeImage.all
    @home_texts = HomeText.find(1)
    @services = Service.all
    set_home_slider(@home_page.slider_id)
    set_promo_slider(@home_page.promo_slider_id)
    set_numbers
  end

  private
  def applicant_params
    params.require(:applicant).permit(:name, :phone, :email, :rut, :work_area,:pdf_file)
  end
end
