class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path

  def set_path
    @path = "products"
    @section = "Productos"
  end

  # GET /products
  # GET /products.json
  def index
    if params[:search]
      s = params[:search]
      @products = Product.where("name LIKE ?","%#{s}%").page params[:page]
    else
      @products = Product.all.page params[:page]
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @title = 'Crear Nuevo Producto'
    @action = 'new'
    @categories = Category.all
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
    @comments = Comment.where(product_id:  @product.id).order(created_at: :desc)
    @categories = Category.all
    @action = 'edit'
    @title = 'Editar Producto'
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        name = product_params[:name].gsub(' ','-').gsub('.','')
        url = @product.brand.name + '/' +name.to_s + '-' + @product.id.to_s
        @product.url = url
        @product.save!
        if product_params[:categories]
          @categories = product_params[:categories].split(%r{,\s*})
          @categories.each do |c|
            ProductCategory.create(:category_id => c,:product_id => @product.id)
          end
        end
        if params[:images]
          params[:images].each { |image|
            @product.images.create(image: image)
          }
        end
        format.html { redirect_to @product, notice: 'El producto ha sido creado correctamente.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        if params[:images]
          Image.where(:product_id => @product.id).destroy_all
          params[:images].each { |image|
            @product.images.create(image: image)
          }
        end
        name = product_params[:name].gsub(' ','-').gsub('.','')
        url = @product.brand.name + '/' +name.to_s + '-' + @product.id.to_s
        @product.url = url
        @product.save!

        if product_params[:categories]
          @categories = product_params[:categories].split(%r{,\s*})
          ProductCategory.where(:product_id => @product.id).destroy_all
          @categories.each do |c|
            ProductCategory.create(:category_id => c,:product_id => @product.id)
          end
        end
        format.html { redirect_to @product, notice: 'El producto ha sido editado correctamente.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    ProductCategory.where(:product_id => @product.id).destroy_all
    Image.where(:product_id => @product.id).destroy_all
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'El producto ha sido eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :price, :description, :avatar, :categories,:status,:images,:info_add,:sku,:pdf_file,:brand_id)
    end
end
