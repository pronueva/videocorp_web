class ChatController < ApplicationController
  include Devise::Controllers::Helpers
  layout "chat"
  before_action :set_pending_chats

  def set_pending_chats
    @pending_chats = Chat.where(status: "2")
  end

  def index
    @current_customer = Customer.new
  end

  def conectar

    if signed_in?
      sign_out( current_user )
    end

    @connected_cms = User.where(chat_user_status:"1")

    @customer_name = customer_params[:name]
    @customer_email = customer_params[:email]
    @customer_company = customer_params[:company]

    save_customer

    if @connected_cms.count > 0

      @chat_room = Chat.new
      @chat_room.status = 2
      @chat_room.details = 'Chat pendiente'
      @chat_room.save!


      @chat_room.customer_id = @customer.id
      @chat_room.room_name = Time.new.year.to_s + Time.new.month.to_s + Time.new.day.to_s + @chat_room.id.to_s
      @chat_room.save!

      @channel = 'channel_chat_requests'
      @event =  'new_request'
      Pusher.trigger(@channel, @event , { :chat_room_id => @chat_room.id,
                                          :chat_room => @chat_room.room_name,
                                          :customer_id => @customer.id,
                                          :customer_email => @customer.customer_email,
                                          :customer_company => @customer.customer_company,
                                          :customer_name => @customer.customer_name,
                                          :pending_chats => @pending_chats.count
                    }) #push a la lista de chats pendientes en el backend


      @response_msg =  'Espere mientras lo conectamos con un monitor'.html_safe
      @status_msg = 1

      render "chat/index"
    else
      @channel = 'channel_cm_status-'+customer_params[:email]
      @event =  'cm_status'

      save_customer

      @response_msg = 'Lo sentimos, en este momento no hay operadores disponibles, intentelo más tarde. <br>Gracias.'.html_safe
      @status_msg = 0

      render "chat/index"
    end
  end



  def accept_chat
    @path = "chat"
    @room = Chat.find(params[:room])
    @customer_email = params[:customer_email]
    @socket_id = params[:sid]
    @customer = params[:customer]
    @cm = params[:cm]
    @room.status = 1
    @room.user_id = @cm
    @room.details = 'Chat activo'
    @room.save!

    if current_user != nil
      @user = User.find(current_user.id)
      @user.chat_user_status = 2
      @user.save!
    end
    @channels = Array[
        'channel_chat_requests-'+@customer_email,
    ]

    @event =  'accepted_connection'

    Pusher.trigger(@channels, @event , { :customer => params[:customer],:cm => params[:cm],:status => 1,:room => @room.room_name})

    render "admin/_chatTable", :layout => false
  end
  def cancel_chat
    @path = "chat"

    @chat = Chat.find(params[:room])
    @chat.status = 4 #rechazado
    @chat.details = 'Chat rechazado'
    @chat.save!

    @channel = 'channel_chat_requests-'+params[:customer_email]
    @event =  'rejected_connection'
    Pusher.trigger(@channel, @event , { :message => 'Lo sentimos, en este momento no hay operadores disponibles, intentelo más tarde. <br>Gracias.',
    }, :socket_id => params[:sid])

    render "admin/_chatTable", :layout => false
  end

  def room
    @room = Chat.find_by_room_name( params[:room] )
    @customer = Customer.find(@room.customer_id)
    @cm = User.find(@room.user_id)

    @messages = ChatMessage.where(chat_id: @room.id)

    @channels = Array[
        'private-channel-chat_' + params[:room],
    ]
    render "chat/_room"
  end

  def messages
    @chat_room = Chat.find_by_room_name(params[:room])

    @message = ChatMessage.new
    @message.body = params[:message] #mensaje
    @message.chat_id = @chat_room.id #room
    @message.user_type_id = params[:user_type] #room
    @message.timestamp = Time.new.hour.to_s + ':' + Time.new.min.to_s  + ':' + Time.new.sec.to_s
    @message.save!


    @messages = ChatMessage.where(chat_id: @chat_room.id)

    @channel = 'private-channel-chat_' + @chat_room.room_name
    @event = 'new-message'

    Pusher.trigger(@channel, @event , { :message => 'Nuevo mensaje' },:socket_id => params[:socket_id])


    render "chat/_messages"
  end

  def get_messages
    @chat_room = Chat.find_by_room_name(params[:room])

    @messages = ChatMessage.where(chat_id: @chat_room.id)

    @channel = 'private-channel-chat_' + @chat_room.room_name
    @event = 'new-message'

    render "chat/_messages"
  end

  def disconnect
    @chat_room = Chat.find_by_room_name(params[:room])
    @customer = Customer.find(params[:customer])
    @customer = User.find(@customer.user_id)
    @cm = User.find(params[:cm])

    @cm.chat_user_status = 1
    @cm.save!
    @chat_room.status = 3
    @chat_room.details = 'Chat inactivo'
    @chat_room.save!

    Pusher.trigger( 'private-channel-chat_' + @chat_room.room_name,'chat-finish',{:message => ' Se ha finalizado la conversación. Gracias'},:socket_id => params[:socket_id])

    render :text => 'desconectando'
  end


  def save_customer
    @customer =  Customer.find_by_customer_email(@customer_email)
    #registrar al cliente en base al correo
    if @customer == nil
      @customer = Customer.new
      @customer.customer_name = @customer_name
      @customer.customer_email = @customer_email
      @customer.customer_company = @customer_company
      @customer.save!
      if !User.find_by_email(@customer_email)
        #create user
        user = User.new
        user.email = @customer_email
        user.password ='123123'
        user.password_confirmation = '123123'
        user.name = @customer_name
        user.last_name = @customer_company
        user.username = @customer_email
        user.user_type_id = '5'
        user.save!
        @customer.user_id = user.id
        @customer.save!

      else
        user =  User.find_by_email(@customer_email)
        @customer.user_id = user.id
        @customer.save!

      end
      sign_in( :user ,  user )
    else
      @customer.customer_name = @customer_name
      @customer.customer_email = @customer_email
      @customer.customer_company = @customer_company
      @customer.save!
      @last_room = Chat.where(status: "2").find_by_customer_id(@customer.id)
      if  @last_room != nil
        @last_room.status = 3 #deshabilitado
        @last_room.save!
      end
      @customer.socket_id = @customer_socket_id
      @customer.save!

      user =  User.find_by_email(@customer_email)
      sign_in( :user , user )
    end
  end



  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def customer_params
    params.require(:customer).permit(:name, :email, :company, :socket_id)
  end
end
