class SliderImagesController < ApplicationController
  before_action :set_slider_image, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path

  def set_path
    @path = "sliders"
    @section = "Sliders"
  end


  # GET /slider_images
  # GET /slider_images.json
  def index
    @slider_images = SliderImage.all.order_asc
  end

  # GET /slider_images/1
  # GET /slider_images/1.json
  def show
  end

  # GET /slider_images/new
  def new
    @slider_image = SliderImage.new
    @slider = Slider.find(params[:slider_id])
  end

  # GET /slider_images/1/edit
  def edit
    @slider = Slider.find(params[:slider_id])
  end

  # POST /slider_images
  # POST /slider_images.json
  def create
    @slider = Slider.find(slider_image_params[:slider_id])
    @slider_image = SliderImage.new(slider_image_params)
    order = @slider.slider_images.count + 1
    respond_to do |format|
      if @slider_image.save
        @slider_image.slider_id = slider_image_params[:slider_id]
        @slider_image.order = order
        @slider_image.save!
        @slider = Slider.find(slider_image_params[:slider_id])
        format.html { redirect_to edit_slider_path(@slider), notice: 'La imagen ha sido subida exitosamente.' }
        format.json { render :show, status: :created, location: @slider_image }
      else
        format.html { render :new }
        format.json { render json: @slider_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slider_images/1
  # PATCH/PUT /slider_images/1.json
  def update
    respond_to do |format|
      if @slider_image.update(slider_image_params)
        @slider_image.slider_id = slider_image_params[:slider_id]
        @slider_image.order = params[:order]
        @slider_image.save!
        @slider = Slider.find(slider_image_params[:slider_id])
        format.html { redirect_to edit_slider_path(@slider), notice: 'La imagen ha sido subida exitosamente.' }
        format.json { render :show, status: :ok, location: @slider_image }
      else
        format.html { render :edit }
        format.json { render json: @slider_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slider_images/1
  # DELETE /slider_images/1.json
  def destroy
    @slider = Slider.find(@slider_image.slider_id)
    @slider_image.destroy
    respond_to do |format|
      format.html { redirect_to  edit_slider_path(@slider), notice: 'La imagen ha sido eliminada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slider_image
      @slider_image = SliderImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slider_image_params
      params.require(:slider_image).permit(:image,:slider_id,:order)
    end
end
