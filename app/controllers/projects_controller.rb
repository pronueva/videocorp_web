class ProjectsController < ApplicationController
  require 'json'
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path
  before_action :set_categories


  def set_categories
    @project_categories = ProjectCategory.all
  end

  def set_path
    @path = "projects"
    @section = "Proyectos"
  end
  # GET /projects
  # GET /projects.json
  def index
    if params[:search]
      s = params[:search]
      @projects = Project.where("name LIKE ?","%#{s}%").page params[:page]
    else
      if params[:project_type]
        @projects = Project.where("project_category_id = ? ",params[:project_type].to_i).page params[:page]
      else
        @projects = Project.all.page params[:page]
      end
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project_categories = ProjectCategory.all
    @project = Project.new
    @title = "Nuevo Proyecto"
    @categories = Category.all
  end

  # GET /projects/1/edit
  def edit
    @title = "Editar Proyecto"
    @project_categories = ProjectCategory.all
    @categories = Category.all
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        if params[:images]
          params[:images].each { |image|
            @project.images.create(image: image)
          }
        end
        if params[:principal_img]
          @project.images.create(image: params[:principal_img])
        end

        name = project_params[:name].gsub(' ','-').gsub('.','')
        url = name.to_s + '-' + @project.id.to_s
        @project.url = url
        @project.save!

        format.html { redirect_to @project, notice: 'El proyecto fue creado correctamente.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    if params[:avatar_projects]
      render :text => params[:avatar_projects]
    end
    respond_to do |format|
      if @project.update(project_params)
        if params[:images]
          Image.where(:project_id => @project.id).destroy_all
          params[:images].each { |image|
            @project.images.create(image: image)
          }
        end

        name = project_params[:name].gsub(' ','-').gsub('.','')
        url = name.to_s + '-' + @project.id.to_s
        @project.url = url
        @project.save!

        format.html { redirect_to @project, notice: 'El proyecto fue editado correctamente' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    Image.where(:project_id => @project.id).destroy_all
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'El proyecto fue eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :date, :customer_name, :description, :avatar_projects,:principal_img, :customer_logo, :project_type, :images, :project_category_id, :is_featured, :involved_areas, :project_video,:testimonial_video,:search)
    end
end
