class PusherController < ApplicationController

  def auth
    if current_user
      response = Pusher.authenticate(params[:channel_name], params[:socket_id])
      render json: response
    else
      render text: 'asd', status: '403'
    end
  end
  
end
