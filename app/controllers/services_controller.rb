class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path

  def set_path
    @path = "services"
    @section = "Servicios"
  end
  # GET /services
  # GET /services.json
  def index
    @services = Service.all.page params[:page]
    @categories = ServiceCategory.all
  end

  # GET /services/1
  # GET /services/1.json
  def show
    @categories = ServiceCategory.all
  end

  # GET /services/new
  def new
    @title = 'Crear Nuevo Servicio'
    @action = 'new'
    @categories = ServiceCategory.all
    @service = Service.new
  end

  # GET /services/1/edit
  def edit
    @title = 'Editar Nuevo Servicio'
    @action = 'edit'
    @categories = ServiceCategory.all
    @service_category = Service.find(params[:id]).service_category
  end

  # POST /services
  # POST /services.json
  def create
    @service = Service.new(service_params)
    @categories = ServiceCategory.all

    respond_to do |format|
      if @service.save
        format.html { redirect_to @service, notice: 'El servicio creado correctamente.' }
        format.json { render :show, status: :created, location: @service }
      else
        format.html { render :new }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    @categories = ServiceCategory.all
    respond_to do |format|
      if @service.update(service_params)
        format.html { redirect_to @service, notice: 'El servicio editado correctamente.' }
        format.json { render :show, status: :ok, location: @service }
      else
        format.html { render :edit }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service.destroy
    respond_to do |format|
      format.html { redirect_to services_url, notice: 'El servicio eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:name, :image, :service_category_id)
    end
end
