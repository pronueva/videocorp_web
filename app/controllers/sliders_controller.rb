class SlidersController < ApplicationController
  require 'json'
  before_action :set_slider, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path

  def set_path
    @path = "sliders"
    @section = "Sliders"
  end

  # GET /sliders
  # GET /sliders.json
  def index
    @sliders = Slider.all.order("slider_type_id ASC").page params[:page]
  end

  # GET /sliders/1
  # GET /sliders/1.json
  def show
  end

  # GET /sliders/new
  def new
    @slider = Slider.new
  end

  # GET /sliders/1/edit
  def edit
    @action = 'edit'
    @title = 'Editar Slider'
    @slider_images = SliderImage.where("slider_id = ?",@slider.id).order_asc.page params[:page]
  end

  # POST /sliders
  # POST /sliders.json
  def create
    @slider = Slider.new(slider_params)
    respond_to do |format|
      if @slider.save
        if slider_params[:images]
          slider_params[:images].each { |image|
            @slider.images.create(image: image)
          }
        end
        format.html { redirect_to @slider, notice: 'El Slider ha sido creado exitosamente.' }
        format.json { render :show, status: :created, location: @slider }
      else
        format.html { render :new }
        format.json { render json: @slider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sliders/1
  # PATCH/PUT /sliders/1.json
  def update
    respond_to do |format|
      if @slider.update(slider_params)
        if slider_params[:images]
          Image.where(:slider_id => @slider.id).destroy_all
          slider_params[:images].each { |image|
            @slider.images.create(image: image)
          }
        end
        format.html { redirect_to @slider, notice: 'El Slider ha sido modificado exitosamente.' }
        format.json { render :show, status: :ok, location: @slider }
      else
        format.html { render :edit }
        format.json { render json: @slider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sliders/1
  # DELETE /sliders/1.json
  def destroy
    @slider.destroy
    respond_to do |format|
      format.html { redirect_to sliders_url, notice: 'El Slider ha sido eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  def slider_video
    @slider_video = SliderImage.new
    @slider = Slider.find(params[:slider_id])
    render "slider_video/_form"
  end
  def slider_video_upload
    render :plain => params

  end
  def slider_video_edit
    @slider_video = SliderImage.find(params[:video_id])
    @slider = Slider.find(params[:slider_id])
    render "slider_video/_form"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slider
      @slider = Slider.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slider_params
      params.require(:slider).permit(:name,:image,:slider_type_id, :slider_video_url, :video)
    end
end
