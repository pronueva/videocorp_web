class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # POST /services
  # POST /services.json
  def create
    @comment = Comment.new(params[:all])
    @product = Product.find(params[:product_id])
    @brand = Brand.find_by_name(@product.url.split("/")[0])
    @product_url = @product.url.split("/")[1]


    respond_to do |format|
    @comment.product_id = params[:product_id]
    @comment.user = params[:user]
    @comment.comment = params[:comment]

      if @comment.save
        format.html { redirect_to  :controller => 'welcome', :action => 'product', :brand => @brand.name , :product_url => @product_url }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    respond_to do |format|
      if @comment.update(params[:all])
        format.html { redirect_to @comment, notice: 'El servicio editado correctamente.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'El comentario ha sido eliminado correctamente.' }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_comment
    @comment = Comment.find(params[:all])
  end

end
