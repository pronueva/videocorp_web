class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path

  def set_path
    @path = "categories"
    @section = "Categorías"
  end
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.where(["id != 5 AND id != 6"]).page params[:page]
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @title = 'Crear Nueva Categoría'
    @action = "new"
    @category = Category.new
    @categories = Category.all
  end

  # GET /categories/1/edit
  def edit
    @title = 'Editar Categoría'
    @categories = Category.all
    @action = "edit"
  end

  # POST /categories
  # POST /categories.json
  def create
    if !Category.where(slug: category_params['slug']).present?
      @category = Category.new(category_params)
      respond_to do |format|
        if @category.save
          format.html { redirect_to @category, notice: 'La categoría ha sido creada correctamente.' }
          format.json { render :show, status: :created, location: @category }
        else
          format.html { render :new }
          format.json { render json: @category.errors, status: :unprocessable_entity }
        end
      end
    else
     redirect_to :back, alert: 'Categoría con url: '+category_params['slug']+' ya existe'
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'La categoría ha sido editada correctamente.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'La categoría ha sido eliminada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :category_id, :desciption, :slug, :image, :icon)
    end
end
