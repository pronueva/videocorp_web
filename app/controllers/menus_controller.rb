class MenusController < ApplicationController
  before_action :set_menu, only: [:show, :edit, :update, :destroy]
  layout "admin_layout"
  before_action :set_path

  def set_path
    @path = "menus"
    @section = "Menus"
  end
  # GET /menus
  # GET /menus.json
  def index
      if params[:search]
        s = params[:search]
        @menus = Menu.where("name LIKE ?","%#{s}%").order('id ASC').page params[:page]
      else
        @menus = Menu.order('id ASC').all.page params[:page]
      end
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
  end

  # GET /menus/new
  def new
    if params[:menu_id]
      @menu_id = params[:menu_id]
      @parent_menu = Menu.find(@menu_id)
    end
    @menu = Menu.new
    @title = 'Nuevo Menu'
  end

  # GET /menus/1/edit
  def edit
    @title = 'Editar Menu'
  end

  # POST /menus
  # POST /menus.json
  def create
    @menu = Menu.new(menu_params)

    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: 'Menu was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to menus_url, notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:name, :url, :menu_id, :status)
    end
end
