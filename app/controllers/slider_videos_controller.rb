class SliderVideosController < ApplicationController
  before_action :set_slider_video, only: [:show, :edit, :update, :destroy]
  before_action :set_path
  layout "admin_layout"

  def set_path
    @path = "sliders"
    @section = "Sliders"
  end


  # GET /slider_video
  # GET /slider_video.json
  def index
    @slider_video = SliderVideo.all.order_asc
  end

  # GET /slider_video/1
  # GET /slider_video/1.json
  def show
  end

  # GET /slider_video/new
  def new
    @slider_video = SliderVideo.new
    @slider = Slider.find(slider_video_params[:slider_id])
    render "slider_video/_form"
  end

  # GET /slider_video/1/edit
  def edit
    @slider = Slider.find(slider_video_params[:slider_id])
  end

  # POST /slider_video
  # POST /slider_video.json
  def create
    @slider = Slider.find(slider_video_params[:slider_id])
    @slider_video = SliderVideo.new(slider_video_params)
    order = @slider.slider_videos.count + 1
    respond_to do |format|
      if @slider_video.save
        @slider_video.slider_id = slider_video_params[:slider_id]
        @slider_video.order = order
        @slider_video.save!
        @slider = Slider.find(slider_video_params[:slider_id])
        format.html { redirect_to edit_slider_video_path(@slider), notice: 'La video ha sido subida exitosamente.' }
        format.json { render :show, status: :created, location: @slider_video }
      else
        format.html { render :new }
        format.json { render json: @slider_video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slider_video/1
  # PATCH/PUT /slider_video/1.json
  def update
    respond_to do |format|
      if @slider_video.update(slider_video_params)
        @slider_video.slider_id = slider_video_params[:slider_id]
        @slider_video.order = slider_video_params[:order]
        @slider_video.save!
        @slider = Slider.find(slider_video_params[:slider_id])
        format.html { redirect_to edit_slider_video_path(@slider), notice: 'La video ha sido subida exitosamente.' }
        format.json { render :show, status: :ok, location: @slider_video }
      else
        format.html { render :edit }
        format.json { render json: @slider_video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slider_video/1
  # DELETE /slider_video/1.json
  def destroy
    @slider = Slider.find(@slider_video.slider_id)
    @slider_video.destroy
    respond_to do |format|
      format.html { redirect_to  edit_slider_path(@slider), notice: 'La video ha sido eliminada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slider_video
      @slider_video = SliderVideo.find(slider_video_params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slider_video_params
      params.require(:slider_video).permit(:video,:slider_id,:order)
    end
end
