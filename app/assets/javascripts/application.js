// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require nprogress
//= require nprogress-turbolinks
//= require ./sweetalert.min.js
//= require ./countup.js
//= require ./slider.js

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function link(url){
    window.location.href = baseUrl() + '/' + url;
}
function baseUrl(){
    var pathArray = location.href.split( '/' );
    var protocol = pathArray[0];
    var host = pathArray[2];
    var url = protocol + '//' + host;

    return protocol + '//' + host;
}
var check = function() {
    var googleResponse = $('#g-recaptcha-response').val();
    var btn = $('#send_btn');
    if (!googleResponse) {
        btn.attr('disabled', true);
    } else {
        btn.attr('disabled', false);
    }
};

$(document).on('ready',function(){
    $('#contactModal').modal('show');
    $(document).on('click','.carousel.slide',function(e){
        e.preventDefault();
    });
    $(document).on('mouseover','.dropdown-toggle',function() {
        $(this).attr('aria-expanded', 'true');
        $(this).parent().addClass('open');

    });
    $(document).on('mouseleave','.dropdown-toggle',function() {
        $(this).attr('aria-expanded', 'false');
        $('.dropdown-menu').hover(
            function () {
                $(this).parent().addClass('open');
            },
            function () {
                $(this).parent().removeClass('open');
            }
        );
        $(this).parent().removeClass('open');
    });

    var options = {
        useEasing : true,
        useGrouping : true,
        separator : ',',
        decimal : '.',
        prefix : '',
        suffix : ''
    };
    if ( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent) ) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 120) {
                $('#nav-default').hide();
                $('#nav-sticky').show();
                $('.home').attr('style', 'margin-top:120px');
                $('#home_slider').attr('style', 'margin-top:120px');
            }
            if ($(this).scrollTop() == 0) {
                $('#nav-default').show();
                $('#nav-sticky').hide();
                $('#home_slider').attr('style', 'margin-top:0px');
                $('.home').attr('style', 'margin-top:0px');
            }
            if (window.location.pathname == '/') {
                var isElementInView = Utils.isElementInView($('#nums'), false);
                var num_q = $('#num_q').val();
                if (isElementInView && num_q == 0) {
                    $('#num_q').val('1');
                    $('.number').each(function () {
                        var target = $(this);
                        var endVal = $(this).data('number');
                        var demo = new CountUp(target.attr('id'), 0, endVal, 0, 3, options);
                        demo.start(function () {
                            demo.update(endVal)
                        });
                        var demo = new CountUp(target.attr('id'), 0, endVal, 0, 3, options);
                        demo.start(function () {
                            demo.update(endVal)
                        });
                    });
                }
            }
        });
    }
    $('#chat-button').click(function(){
        window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=900,height=550');
        return false
    });

    $(".messages-box").animate({ scrollTop: $('.messages-box').prop("scrollHeight")}, 1000);

    $('#btn_search1').on('click',function(){
       var keywords = $('#search_header1').val();
        $('#search_header_form1').submit();
    });
    $('#btn_search2').on('click',function(){
       var keywords = $('#search_header2').val();
        $('#search_header_form2').submit();
    });
    $('#btn_search3').on('click',function(){
       var keywords = $('#search_footer').val();
        $('#search_footer_form').submit();
    });


    // navbar responsive
    var count = 0;
    $(document).on('click','.btn-mobile-search',function(e){
        e.preventDefault();
        if( count == 0 ){
            $('.search_header_form1').fadeIn('fast');
            count = 1;
        }else{
            $('.search_header_form1').fadeOut('fast');
            count = 0;
        }
    });
    $(document).on('click','.navbar-toggle',function(){
        var expanded = $('#navbar-collapse-2').attr("aria-expanded");

        if( expanded == "false" ){
            $('.top-menu').attr('style','opacity:0');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(1)').attr('style','');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(2)').attr('style','');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(3)').attr('style','');

        }else{
            $('.top-menu').each(function( k , el){
                $(this).attr('style','opacity:1');
            })
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(1)').attr('style','transform: rotateX(5deg) rotateY(45deg) rotateZ(38deg); margin-bottom:-6px');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(2)').attr('style','transform: rotateX(-10deg) rotateY(-25deg) rotateZ(-45deg)');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(3)').attr('style','opacity:0');

        }
    });


    $(document).on('click','.plus',function(e){
        e.preventDefault();
        $(this).html('<i class="fa fa-minus"></i>');
        $(this).addClass('minus');
        $(this).removeClass('plus');
    });
    $(document).on('click','.minus',function(e){
        e.preventDefault();
        $(this).html('<i class="fa fa-plus"></i>');
        $(this).addClass('plus');
        $(this).removeClass('minus');
        $(this).parent().removeClass('open');
    });
});


function Utils() {}

Utils.prototype = {
    constructor: Utils,
    isElementInView: function (element, fullyInView) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
            return ((pageTop < elementTop) && (pageBottom > elementBottom));
        } else {
            return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
    }
};
var Utils = new Utils();