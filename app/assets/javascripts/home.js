// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require ./countup.js
//= require ./sweetalert.min.js

$(document).on('mouseover','.dropdown-toggle',function() {
    $(this).attr('aria-expanded', 'true');
    $(this).parent().children('.dropdown-menu').addClass('show');

});
$(document).on('mouseleave','.dropdown-toggle',function() {
    $(this).attr('aria-expanded', 'false');
    $('.dropdown-menu').hover(
        function () {
            $(this).parent().children('.dropdown-menu').addClass('show');
        },
        function () {
            $(this).parent().children('.dropdown-menu').removeClass('show');
        }
    );
    $(this).parent().children('.dropdown-menu').removeClass('show');
});

$(document).on('mouseover','.dropdown-menu li', function() {
    $(this).addClass('active');
});
$(document).on('mouseleave','.dropdown-menu li', function() {
    $(this).removeClass('active');
});

$(document).on("turbolinks:load", function() {
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    function link(url){
        window.location.href = baseUrl() + '/' + url;
    }
    function baseUrl(){
        var pathArray = location.href.split( '/' );
        var protocol = pathArray[0];
        var host = pathArray[2];
        var url = protocol + '//' + host;

        return protocol + '//' + host;
    }
    var check = function() {
        var googleResponse = $('#g-recaptcha-response').val();
        var btn = $('#send_btn');
        if (!googleResponse) {
            btn.attr('disabled', true);
        } else {
            btn.attr('disabled', false);
        }
    };
    // navbar responsive
    var count = 0;
    $(document).on('click','.btn-mobile-search',function(e){
        e.preventDefault();
        if( count == 0 ){
            $('.search_header_form1').fadeIn('fast');
            count = 1;
        }else{
            $('.search_header_form1').fadeOut('fast');
            count = 0;
        }
    });
    $(document).on('click','.navbar-toggle',function(){
        var expanded = $('#navbar-collapse-2').attr("aria-expanded");

        if( expanded == "false" ){
            $('.top-menu').attr('style','opacity:0');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(1)').attr('style','');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(2)').attr('style','');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(3)').attr('style','');

        }else{
            $('.top-menu').each(function( k , el){
                $(this).attr('style','opacity:1');
            })
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(1)').attr('style','transform: rotateX(5deg) rotateY(45deg) rotateZ(38deg); margin-bottom:-6px');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(2)').attr('style','transform: rotateX(-10deg) rotateY(-25deg) rotateZ(-45deg)');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(3)').attr('style','opacity:0');

        }
    });
    $('.service-').parent().each(function () {
        var parent = $(this).parents('.service-container');
        var image = parent.find('.service-image');
        var newSrc = $(this).children('.service-').data('url');
        if($(this).hasClass('active')) {
            image.attr('src',newSrc);
        }
        $(this).on('click', function(e) {
            e.preventDefault();
            $('#service-image-modal').attr('src',newSrc);
        });
        $(this).hover(
            function () {
                parent.find('.item').each(function(){$(this).removeClass('active')});
                $(this).addClass('active');
                image.fadeOut(80, function () {
                    image.attr('src',newSrc);
                })
                    .fadeIn(50)
            },
            function () {

            }
        );
    });
    if ( ! /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent) ) {
        var options = {
            useEasing : true,
            useGrouping : true,
            separator : ',',
            decimal : '.',
            prefix : '',
            suffix : ''
        };
        $(window).scroll(function () {
             if (window.location.pathname == '/' || window.location.pathname == '/nuevo') {
                var isElementInView = Utils.isElementInView($('#home-numbers'), false);
                var num_q = $('#num_q').val();
                if (isElementInView && num_q == 0) {
                    $('#num_q').val('1');
                    $('.number').each(function () {
                        var target = $(this);
                        var endVal = $(this).data('number');
                        var demo = new CountUp(target.attr('id'), 0, endVal, 0, 3, options);
                        demo.start(function () {
                            demo.update(endVal)
                        });
                        var demo = new CountUp(target.attr('id'), 0, endVal, 0, 3, options);
                        demo.start(function () {
                            demo.update(endVal)
                        });
                    });
                }
            }
        });
    }
    $('#youtube-link').on('click', function() {
        window.location.href = "https://www.youtube.com/channel/UCm_3HQyszGXoH7aixyytVKg/videos";
    });
    setInterval( function () {
        $(".flip-card-content").toggleClass("flip");
    }, 4000);
});

function Utils() {}
Utils.prototype = {
    constructor: Utils,
    isElementInView: function (element, fullyInView) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
            return ((pageTop < elementTop) && (pageBottom > elementBottom));
        } else {
            return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
    }
};
var Utils = new Utils();