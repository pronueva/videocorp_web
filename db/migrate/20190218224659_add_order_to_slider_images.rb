class AddOrderToSliderImages < ActiveRecord::Migration[5.2]
  def change
    add_column :slider_images, :order, :integer
  end
end
