class AddVideoUrlToSliderImages < ActiveRecord::Migration[5.2]
  def change
    add_column :slider_images, :video_url, :string
  end
end
