class CreateSliderVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :slider_videos do |t|
      t.attachment :video
      t.integer :slider_id
      t.integer :order

      t.timestamps
    end
  end
end
