class AddIsVideoToSliderImages < ActiveRecord::Migration[5.2]
  def change
    add_column :slider_images, :is_video, :boolean
  end
end
