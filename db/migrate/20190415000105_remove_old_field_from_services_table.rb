class RemoveOldFieldFromServicesTable < ActiveRecord::Migration[5.2]
  def change
    remove_column :services, :service_id
    remove_column :services, :slug
    remove_column :services, :description
    remove_column :services, :status
    remove_column :services, :category_id
  end
end
