class AddServiceCategoryIdToServicesTable < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :service_category_id, :integer
  end
end
