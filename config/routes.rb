Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'

  # get 'contacto/:sucursal' => 'front/contact#contacto'
  get 'contacto' => 'front/contact#contacto'
  get 'trabaja-con-nosotros' => 'front/contact#work_with_us'
  post 'contacto/:sucursal' => 'front/contact#send_contact'
  post 'trabaja-con-nosotros' => 'front/contact#send_work_with_us'

  devise_for :users,
             path: 'admin/auth',
             path_names: { sign_in: 'sign_in', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }

  #ADMIN routes
  authenticated :user do
    get 'admin' => 'admin#home', as: :user_root
    get 'admin/home' => 'admin#home_page'
    post 'admin/numbers/update' => 'admin#numbers_update'
    post 'admin/home_slider/update' => 'admin#home_slider_update'
    post 'admin/home_promo_slider/update' => 'admin#home_promo_slider_update'
    post 'admin/home_images/update' => 'admin#home_images_update'
    post 'admin/home_texts/update' => 'admin#home_texts_update'
    resources :products, path: '/admin/products'
    resources :categories, path: '/admin/categories'
    resources :services, path: '/admin/services'
    resources :users, path: '/admin/users'
    resources :projects, path: '/admin/projects'
    resources :pages, path: '/admin/pages'
    resources :partners, path: '/admin/partners'
    resources :sliders, path: '/admin/sliders' do

    end
    resources :slider_images, path: '/admin/slider'
    resources :slider_videos, path: '/admin/slider/video'
    resources :brands, path: '/admin/brands'
    resources :menus, path: '/admin/menus'

    delete 'comment/delete/:all' => 'comments#destroy'

    get 'search' => 'welcome#search'
    get 'admin/chat' => 'admin#chat'
    get 'admin/chat/change_status' => 'admin#change_status'
    get 'admin/chat/refresh_table' => 'admin#refresh_table'
    get 'admin/chat/cancel_chat' => 'chat#cancel_chat'
    get 'admin/chat/accept_chat' => 'chat#accept_chat'

    get 'chat/room/:room' => 'chat#room'
    post 'chat/send_message' => 'chat#messages'
    post 'chat/get_messages' => 'chat#get_messages'
    post 'chat/disconnect' => 'chat#disconnect'
  end
  unauthenticated :user do
    get  'admin' => 'admin#auth'
    get  'productos/search' => 'welcome#home'
    get  'admin/products/:all' => 'admin#home'
    get  'admin/products' => 'admin#home'
    get  'admin/products/:all/edit' => 'admin#home'
    get  'admin/products/new' => 'admin#home'
    get  'admin/categories/:all' => 'admin#auth'
    get  'admin/categories' => 'admin#auth'
    get  'admin/categories/new' => 'admin#auth'
    get  'admin/categories/:all/edit' => 'admin#auth'
    get  'admin/services/:all' => 'admin#auth'
    get  'admin/services' => 'admin#auth'
    get  'admin/services/new' => 'admin#auth'
    get  'admin/services/:all/edit' => 'admin#auth'
    get  'admin/projects/:all' => 'admin#auth'
    get  'admin/projects' => 'admin#auth'
    get  'admin/projects/new' => 'admin#auth'
    get  'admin/projects/:all/edit' => 'admin#auth'
    get  'admin/menus' => 'admin#auth'
    get  'admin/menus/new' => 'admin#auth'
    get  'admin/menus/:all/edit' => 'admin#auth'

  end

  #PAGINAS INTERIORES

  post 'comment/create' => 'comments#create'
  get 'holding' => 'front/partners#holding'
  get 'proyectos-ejecutados' => 'front/projects#proyectos_ejecutados'
  get 'proyectos-ejecucion' => 'front/projects#proyectos_ejecucion'

  post 'pusher/auth' => 'pusher#auth'
  get  'chat_room' => 'chat#index'
  get 'conectar' => 'chat#conectar'
  #mostrar servicios

  #mostrar products
  get  '/productos'=> 'welcome#index'
  get 'productos/:brand/:product_url' => 'welcome#index'
  #search products
  get  'productos/:brand' => 'welcome#index'

  #mostrar proyectos
  get 'proyectos/search' => 'front/projects#search_projects'
  get 'proyectos-ejecutados/search' => 'front/projects#search_projects'
  get 'proyectos-ejecutados/:project_url' => 'front/projects#show_proyecto'
  get 'proyectos-ejecucion/:project_url' => 'front/projects#show_proyecto'
  if Rails.env.development?
    get 'upurls' => 'front/projects#update_project_urls'

  end
  get '/:page_name/' => 'front/interior_pages#interior_pages' , :except => ['contacto','trabaja-con-nosotros','/' ,'porque-videocorp' ]
  get '/:page_name/:sub_page' => 'front/interior_pages#interior_pages' , :except => ['contacto','trabaja-con-nosotros','/' ,'porque-videocorp']
end
